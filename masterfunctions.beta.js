/*
 * Project: Generic reusable functions for Adnet websites
 * Author: Rudy Affandi
 * Created: 03/19/2011
 * Revision: 01/06/2012
 * Version: 2.0 (BETA)
 * File name: masterfunctions.beta.js
 */

// jQuery initialization and CSS settings, waiting for DOM tree to initialize
$(document).ready(function(){

   // Add ID for each of the main navigation
   $('nav > ul > li').each(function(index, element){
      $(element).attr('id', 'n'+index);
   });

   // Side navigation 3rd level settings, please set trigger from local functions.js
   if ($('aside ul').length > 0){
      $('aside ul ul').each(function(index, element){
         $(element).attr('id', 's'+index).prev().addClass('has_sub');
      });
   } else {
      $('aside ul ul').each(function(index, element){
         $(element).attr('id', 's'+index);
      });
   }

   // Activate "class='active'" on current URL
   var host = location.host;
   if (jQuery().url){// Check if plugin loaded
      var script_path_length = (jQuery.url.segment(0).length + 2);
   } else {// if no plugin
      var script_path_length = 3;
   }
   var path = location.pathname.substring(script_path_length);
   var jpath = $(location).attr('href');
   var param_path = location.search;
   var complete_path = (path.toLowerCase() + param_path);
   if ( complete_path )
   {
      $('a[href$="' + complete_path + '"]').each(function(){
         $(this).addClass('active current');
         $(this).closest('li').addClass('current');
      });
   }

   // Add ID for each of the sub navigation and marked with 'has_sub' class
   // Show side navigation with children
   $('a.has_sub.active').each(function(){
      $(this).next().show();
   });

   // Also expand tree for all parents
   $('aside li a.active').parents().show();
   $('aside ul ul li a.active').parent().parent().show();
   $('aside ul ul ul li a.active').parent().parent().parent().show();
   $('aside ul ul:visible').prev().addClass('active');
   $('aside ul ul ul:visible').prev().prev().addClass('active');

   // Style default RFI form (Experimental)
   // First we clean up the mess from original table formatting
   // jQuery Uniform plugin required, loaded via AJAX

   if ($('div.form_wrapper').length > 0){
      $.getScript('http://repository.lesaff.com/scripts/jquery_plugins/uniform/jquery.uniform.min.js', function(){
         $('div.form_wrapper table').addClass('ui-helper-reset').removeAttr('background').removeAttr('border');
         $('div.form_wrapper td').removeAttr('background');
         // and then we apply modifier to form elements
         $('div.form_wrapper table input[type=text], div.form_wrapper table td').addClass('ui-corner-all');
         //$('div.form_wrapper table input[type=submit], div.form_wrapper table input[type=reset]').button();
         $('div.form_wrapper table textarea').addClass('ui-corner-all');
         $('select, input:checkbox, input:radio, input:file').uniform({resetSelector: 'input[type="reset"]'});
      });
   }

   // Validate form
   if ($('#rfiform').length > 0){
      $('#rfiform').validate();
      //$('#rfiform input:text:first').focus();
   }

   // Form required fields toggle
   $('a.form_toggle').click(function(){
      $('.form_opt').slideToggle(400);
      $(this).text($(this).text() == 'Click here to show required fields only *' ? 'Click here to show all fields' : 'Click here to show required fields only *');
      return false;
   });

   // Corporate Directory formatting, hide empty fields
   $('.colon').append(':');
   $('.comma').append(',');
   $('div.address div:has(span.value:empty)').hide();
   $('div.address div:has([href]:empty)').hide();
   $('div.address div:has(a.url:empty)').hide();

   // Media Coverage formatting, hide empty fields
   $('div.media_format div:has(span.value:empty)').hide();
   $('div.media_format div:has(span.img:empty)').hide();
   $('div.media_format div:has([href]:empty)').hide();
   $('div.media_format div:has(a.url:empty)').hide();

   // jQuerize Buttons
   //$('button').button();

   // Initiate shadowbox
   //   Shadowbox.init();

   // Convert email href to mailto
   $('a.email').each(function(){
      var email = $(this).html().replace(/\s*\(.+\)\s*/, "@");
      $(this).before('<a href="mailto:' + email + '" rel="nofollow" title="Email ' + email + '">' + email + '</a>').remove();
   });

   // External link warning dialog
   $('a.ext_link').bind('click', function(){
      var link = $(this).attr('href');
      $('<div>You are leaving ' + companyName + ' site. Do you want to proceed?</div>').dialog({
         title: companyName,
         width: 400,
         modal : true,
         overlay: {
            backgroundColor: '#000',
            opacity: 0.5
         },
         buttons: {
            'Proceed': function() {
               $(this).dialog('close').remove();
               window.open(link);
            },
           'Cancel': function() {
              $(this).dialog('close').remove();
              return false;
            }
         }
      });
      return false;
   });

   // Alert dialog box
   $('a.coming_soon').live('click', function(){
      $('<div>Coming soon...</div>').dialog({
         title: companyName,
         width: 300,
         modal : true,
         overlay: {
            backgroundColor: '#000',
            opacity: 0.5
         },
         buttons: {
           'OK': function() {
              $(this).dialog('close').remove();
              return false;
            }
         }
      });
      return false;
   });

   // Image thumbnail processing
   $('ul.image_grid_format li').each(function(){
      var _this = $(this);
      $(this).find('img').load(function(){
         _this.css('width', $(this).width() + 'px');
         _this.find('img').wrap('<div class="zoom"></div>');
         _this.find('.zoom').append('<span></span>');
      });
   });

   $('.img_thumb').each(function(){
      var _this = $(this);
      $(this).find('img').load(function(){
         _this.css('width', $(this).width() + 'px');
         _this.find('img').wrap('<div class="zoom"></div>');
         _this.find('.zoom').append('<span></span>');
      });
   });

	// Zebra-striped tables
	$('table tr:odd').addClass('odd');

});

// Remove qTip on unload
$(window).unload(function() {
   if (jQuery().qtip){// Check if plugin loaded
      $('area').qtip("destroy");
   }
});

// jQuery REGEX filter
jQuery.expr[':'].regex = function(elem, index, match) {
   var matchParams = match[3].split(','),
   validLabels = /^(data|css):/,
   attr = {
      method: matchParams[0].match(validLabels) ?
      matchParams[0].split(':')[0] : 'attr',
      property: matchParams.shift().replace(validLabels,'')
   },
   regexFlags = 'ig',
   regex = new RegExp(matchParams.join('').replace(/^\s+|\s+$/g,''), regexFlags);
   return regex.test(jQuery(elem)[attr.method](attr.property));
}